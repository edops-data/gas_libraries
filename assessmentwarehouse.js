//var _ = Underscore.load();
var _ = eval(UrlFetchApp.fetch('http://underscorejs.org/underscore.js').getContentText());

/**
 * Returns an MD2 hash of the given string value.
 *
 * @param {string} input The string to hash.
 * @return The hashed string (MD2).
 * @customfunction
 */
function hash(s) {
  if (s.map) {
    return s.map(hash);
  } else {
    var hexstr = '';
    var digest = Utilities.computeDigest(Utilities.DigestAlgorithm.MD2, s);
    for (i = 0; i < digest.length; i++) {
      var val = (digest[i]+256) % 256;
      hexstr += ('0'+val.toString(16)).slice(-2);
    }
    return hexstr;
  }
}

/**
 * Returns only values from an array that correspond to nonblank
 * values in the key array.
 *
 * @param {range} input The array to filter.
 * @param {range} input The 1-D array to use as keys.
 * @return The filtered array.
 * @customfunction
 */
function nonblank(data_array, key_array) {
  var f = _.filter(key_array, function(x){return x != '';});
  return _.first(data_array, f.length);
  
}

/**
 * Returns keys for an array of items.
 *
 * @param {range} input The range of items.
 * @param {range} input The 2-D array of [key, item].
 * @return The array of keys.
 * @customfunction
 */
function keymatch(types_array, keys_array) {
  var k = _.unzip(keys_array);

  function get_type(t, keys) {
    return keys[0][_.indexOf(keys[1], _.find(keys[1], function(x){return x == t;}))];
  }
  
  if (typeof(types_array) != 'object'){
    return get_type(types_array, k);
  } else {
    return _.map(types_array, function(x){return get_type(x, k);});
  }
    
}


/**
 * Filters a range by a regular expression.
 *
 * @param {range} input The range to filter.
 * @param {string} input Regular Expression.
 * @return The filtered range.
 * @customfunction
 */
function regexfilter(data_range, filter_text) {
 
  var r = new RegExp(filter_text);
  
  return _.filter(data_range, function(x){return r.exec(x) !== null;});
  
}

/**
 * Simplified VLOOKUP.
 *
 * @param {cell} input The value to find.
 * @param {range} input The range with keys.
 * @param {range} input The range with values.
 * @return The looked up value.
 * @customfunction
 */
function quicklookup(target_value, key_range, target_range) {
 
  function ql(v, r){
    var k = _.map(r, function(x){return type_map(v).constructor(x);});
    var i = _.findIndex(k, function(x){return x == v});
    return target_range[i];
  }
  
  // Convert the key_range to the data type of target_value
  function type_map(c){
    // Try to convert to a number. If it fails, it's a string for our purposes.
    if (isNaN(Number(c))){
      c = String(c);
    }
    // Converting to a number didn't fail, so it must be a number.
    else {
      c = Number(c);
    }
    return c;
  }
  
  // If one cell is being passed, evaluate directly.
  if (typeof(target_value) != 'object'){ 
    return ql(target_value, key_range);
  } 
  // If multiple cells are being passed, they lose their data types.
  else {
    // Loop through the cells, identifying the data type for each.
    return _.map(target_value, function(x){return ql(type_map(x), key_range);});
  }
  
}

/**
 * Fills a value down a range, as long as matching cells are nonblank.
 *
 * @param {cell} input The value to fill.
 * @param {range} input The range of keys.
 * @return The value, filled down the appropriate number of cells
 * @customfunction
 */
function filldown(target_value, key_range) {
 
  var f = _.filter(key_range, function(x){return x != '';});
  
  if (typeof(target_value) != 'object'){
    return _.map(f, function(x){return target_value;});
  } else {
    return _.map(f, function(x){return target_value[0];});
  }
  
}

/**
 * Iterates every item in the column array against every item in the row array.
 *
 * @param {range} input The column array.
 * @param {range} input The row array.
 * @return Iterated array.
 * @customfunction
 */
function unpivot(column_array, row_array){
  var z = [];
  var m = [];
  
  var t = _.unzip(row_array);
  
  function r_iter(c, r){
    if (typeof(row_array) != 'object') {
      return c;
    } else{
      return _.map(r, function(x){return c;});
    }
  }
  
  for (i = 0; i < column_array.length; i++){
    if (typeof(row_array) != 'object') {
      z.push(r_iter(column_array[i], row_array));
      m.push(row_array);
    } else {
      z.push(r_iter(column_array[i], t));
      m.push(t);
    }   
    
  }

  return _.zip(_.flatten(z), _.flatten(m));

}

/**
 * Iterates every item in the column array against every item in the
 * corresponding row of the data array.
 *
 * @param {range} input The column array.
 * @param {range} input The data array.
 * @return Iterated array.
 * @customfunction
 */
function unpivots(column_array, data_array){
  var z = [];
  var m = [];
  
  //var t = _.unzip(data_array);
  
  function r_iter(c, r){
    return _.map(r, function(x){return c;});
  }
  
  for (i = 0; i < column_array.length; i++){
    z.push(r_iter(column_array[i], data_array[i]));
    m.push(data_array[i]);  
    
  }

  return _.zip(_.flatten(z), _.flatten(m));

}
